#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>
#include <time.h>


#include "point.h"
#include "vector.h"
#include "kmeans.h"

#define K 32
#define NUM_POINTS 1000000

void 
print_points(const struct vector *means)
{
    for (size_t i = 0; i < vec_size(means); i++) {
        struct point *p = vec_at(means, i);
        printf("[%*lu] x = %*.3f, y = %*.3f, z = %*.3f\n", 3, i, 6, p->x, 6, p->y, 6, p->z);
    }
}

int 
main(int argc, char *argv[])
{
    struct vector *old_means;
    struct vector *new_means;
    struct vector *points;
    size_t k;
    size_t n;

    k = atoi(argv[1]);
    n = atoi(argv[2]);

    points = generate_points(k, n);
    old_means = init_means(k, points);
    new_means = vec_init(vec_size(old_means));
    vec_copy(new_means, old_means);

    printf("Old means :\n");
    print_points(old_means);

    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    kmeans(old_means, new_means, points);
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);

    printf("\nNew means :\n");
    print_points(new_means);

    uint64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
    printf("\nTIME : %ld us\n", delta_us);

    vec_free(old_means);
    vec_free(new_means);
    vec_free(points);

    return EXIT_SUCCESS;
}