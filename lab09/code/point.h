#pragma once

#include <stdlib.h>


struct point {
    double x;
    double y;
    double z;
};

#include "vector.h"

static inline __attribute__((always_inline)) double
squared_distance(const struct point *a, const struct point *b)
{
    double diff_x = a->x - b->x;
    double diff_y = a->y - b->y;
    double diff_z = a->z - b->z;

    return diff_x * diff_x + diff_y * diff_y + diff_z * diff_z;
}

static inline __attribute__((always_inline)) 
struct vector *
generate_points(const size_t k, const size_t num_points)
{
    srand(2021);

    struct vector *points = vec_init(num_points);

    for (size_t i = 0; i < num_points; i++) {
        struct point p = {
            .x = (i + 1) % k * 10 / k - 5.0 + ((double)rand())/(RAND_MAX/10),
            .y = (i + 5) % k * 10 / k - 5.0 + ((double)rand())/(RAND_MAX/10),
            .z = (i + 7) % k * 10 / k - 5.0 + ((double)rand())/(RAND_MAX/10),
        };

        vec_push(points, p);
    }

    return points;
}

static inline __attribute__((always_inline)) 
struct vector *
init_means(const size_t k, const struct vector *points)
{
    struct vector *means = vec_init(k);

    for (size_t i = 0; i < k; i++) {
        vec_push(means, *vec_at(points, rand() % vec_size(points)));
    }

    return means;
}