/* 08.06.2020 - Sydney Hauke - HPC - REDS - HEIG-VD */

#include <benchmark/benchmark.h>

extern "C" {
    #include "point.h"
    #include "vector.h"
    #include "kmeans.h"
}

#define NUM_POINTS 1000000

class KmeansFixture : public benchmark::Fixture {
public:
    void SetUp(const ::benchmark::State& state)
    {
        points = generate_points(state.range(0), state.range(1));
        old_means = init_means(state.range(0), points);
        new_means = vec_init(state.range(0));
        vec_copy(new_means, old_means);
    }

    void TearDown(const ::benchmark::State&)
    {
        vec_free(points);
        vec_free(old_means);
        vec_free(new_means);
    }

protected:
    struct vector *points;
    struct vector *old_means;
    struct vector *new_means;
};

BENCHMARK_DEFINE_F(KmeansFixture, kmeans_test)(benchmark::State& state)
{
    while (state.KeepRunning()) {
        kmeans(old_means, new_means, points);
    }

}

static void custom_arguments(benchmark::internal::Benchmark *b)
{
    for (size_t i = 0; i <= 5; i++) {
        b->Args({(1 << i), (1 << 20)});
    }
}

BENCHMARK_REGISTER_F(KmeansFixture, kmeans_test)
    ->Apply(custom_arguments)
    ->Unit(::benchmark::kMicrosecond);

BENCHMARK_MAIN();
