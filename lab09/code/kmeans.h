#pragma once

void
kmeans(struct vector *old_means, 
       struct vector *new_means, 
       const struct vector *points);