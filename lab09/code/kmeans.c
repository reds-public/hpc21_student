#include <stdlib.h>
#include <math.h>

#include "point.h"
#include "vector.h"

#define ETA 0.001

static size_t
find_nearest(const struct point p, const struct vector *means) 
{
    size_t nearest_idx = 0;
    double nearest = INFINITY;

    for (size_t i = 0; i < vec_size(means); i++) {
        struct point *mean = vec_at(means, i);

        double distance = squared_distance(&p, mean);

        if (nearest > distance) {
            nearest = distance;
            nearest_idx = i;
        }
    }

    return nearest_idx;
}

static void
classify(struct vector **classified, const struct vector *points, const struct vector *means)
{
    for (size_t i = 0; i < vec_size(points); i++) {
        size_t nearest_idx = find_nearest(*vec_at(points, i), means);

        vec_push(classified[nearest_idx], *vec_at(points, i));
    }
}

static struct point
average_of(const struct vector *classified_points)
{
    double x = 0;
    double y = 0;
    double z = 0;

    for (size_t i = 0; i < vec_size(classified_points); i++) {
        struct point *p = vec_at(classified_points, i);

        x += p->x;
        y += p->y;
        z += p->z;
    }

    struct point mean_point = {
        .x = x / vec_size(classified_points), 
        .y = y / vec_size(classified_points),
        .z = z / vec_size(classified_points)
    };

    return mean_point;
}

static void
update(struct vector *new_means, struct vector **classified)
{
    for (size_t i = 0; i < vec_size(new_means); i++) {
        *vec_at(new_means, i) = average_of(classified[i]);
    }
}

static int
converged(const struct vector *old_means, const struct vector *new_means, double eta)
{
    for (size_t i = 0; i < vec_size(old_means); i++) {
        if (squared_distance(vec_at(old_means, i), vec_at(new_means, i)) > eta)
            return 0;
    }

    return 1;
}

void
kmeans(struct vector *old_means, 
       struct vector *new_means, 
       const struct vector *points)
{
    struct vector **classified;

    classified = (struct vector **)malloc(sizeof(struct vector*) * vec_size(old_means));
    if (classified == NULL) {
        fprintf(stderr, "could not allocate classified\n");
        abort();
    }

    for (size_t i = 0; i < vec_size(old_means); i++) {
        classified[i] = vec_init(0);
    }

    do {
        for (size_t i = 0; i < vec_size(old_means); i++) {
            vec_clear(classified[i]);
        }

        classify(classified, points, old_means);
        update(new_means, classified);
        if (converged(old_means, new_means, ETA))
            break;

        /* New means become old means */
        struct vector *tmp = old_means;
        old_means = new_means;
        new_means = tmp;
    } while (1);

    for (size_t i = 0; i < vec_size(old_means); i++) {
        vec_free(classified[i]);
    }

    free(classified);
}