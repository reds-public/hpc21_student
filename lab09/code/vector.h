#pragma once

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#include "point.h"

#define DEFAULT_CAPACTIY 256

/* Specialized vector of points */
struct vector {
    size_t size;
    size_t capacity;

    struct point *data;
};

static inline __attribute__((always_inline)) struct vector *
vec_init(size_t capacity)
{
    struct vector *vec;

    vec = (struct vector *)malloc(sizeof(struct vector));
    if (vec == NULL) {
        return NULL;
    }

    if (capacity == 0)
        capacity = DEFAULT_CAPACTIY;

    vec->data = (struct point *)malloc(sizeof(struct point) * capacity);
    if (vec->data == NULL) {
        free(vec);
        return NULL;
    }

    vec->capacity = capacity;
    vec->size = 0;

    return vec;
}

static inline __attribute__((always_inline)) void
vec_free(struct vector *vec) 
{
    free(vec->data);
    free(vec);
}

static inline __attribute__((always_inline)) size_t
vec_size(const struct vector *vec)
{
    return vec->size;
}

static inline __attribute__((always_inline)) void 
vec_resize(struct vector *vec)
{
    size_t old_capacity = vec->capacity;

    vec->data = (struct point *)realloc(vec->data, sizeof(struct point) * old_capacity * 2);
    if (vec->data == NULL) {
        fprintf(stderr, "could not resize vector\n");
        abort();
    }

    vec->capacity = old_capacity * 2;
}

static inline __attribute__((always_inline)) void
vec_push(struct vector *vec, struct point p)
{
    if (vec->size == vec->capacity) {
        vec_resize(vec);
    }

    vec->data[vec->size] = p;
    vec->size++;
}

static inline __attribute__((always_inline)) struct point *
vec_at(const struct vector *vec, size_t pos)
{
    return &vec->data[pos];
}

static inline __attribute__((always_inline)) void 
vec_clear(struct vector *vec)
{
    vec->size = 0;
}

static inline __attribute__((always_inline)) void 
vec_copy(struct vector *dest, const struct vector *src)
{
    vec_clear(dest);

    for (size_t i = 0; i < vec_size(src); i++) {
        vec_push(dest, *vec_at(src, i));
    }
}