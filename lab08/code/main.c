#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "image.h"
#include "img_proc.h"

#define EXPECTED_NB_ARGS 4

void print_usage(char *progname)
{
    printf("%s (--sobel|--dither) input_path output_path\n", progname);
}

int main(int argc, char *argv[])
{
    struct img_t *input_img;
    struct img_t *result_img;

    if (argc != EXPECTED_NB_ARGS) {
        fprintf(stderr, "Invalid number of arguments\n");
        print_usage(argv[0]);
        return EXIT_FAILURE;
    }

    input_img = load_image(argv[2]);
    if (!strcmp(argv[1], "--sobel"))
	    result_img = edge_detection(input_img);
    else if (!strcmp(argv[1], "--dither"))
	    result_img = dither(input_img);
    else {
        fprintf(stderr, "Invalid parameter\n");
        print_usage(argv[0]);
        return EXIT_FAILURE;
    }

    save_image(argv[3], result_img);

    free_image(input_img);
    free_image(result_img);

    return EXIT_SUCCESS;
}
